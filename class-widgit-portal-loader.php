<?php
/**
 * Plugin Name:     Portal
 * Plugin URI:      https://widgit.io
 * Description:     The core of the Widgit platform
 * Author:          Widgit Team
 * Author URI:      https://widgit.io
 * Version:         1.0.0
 * Text Domain:     portal
 * Domain Path:     languages
 *
 * @package         Widgit\Portal\Bootstrap
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Conditionally load the plugin.
if ( ! class_exists( 'Widgit_Portal' ) ) {
	$base_dir = trailingslashit( dirname( __FILE__ ) );

	if ( file_exists( $base_dir . 'class-widgit-portal.php' ) ) {
		require_once $base_dir . 'class-widgit-portal.php';
	} else {
		$files = glob( $base_dir . '**/class-widgit-portal.php' );

		if ( 1 === count( $files ) ) {
			require_once $files[0];
		}
	}
}
