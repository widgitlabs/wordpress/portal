<?php
/**
 * Disable the Gutenberg block editor
 *
 * Sorry, Automattic, I can't stand Gutenberg. This tweak disables Gutenberg
 * completely.
 *
 * @package     Widgit\Portal\Modules\DisableGutenberg
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Initialize the Disable Gutenberg module
 *
 * @since       1.0.0
 * @return      void
 */
function widgit_portal_disable_gutenberg_init() {
	if ( widgit_portal_disable_gutenberg_load() ) {
		require_once WIDGIT_PORTAL_DIR . 'includes/modules/disable-gutenberg/filters.php';

		$disable = false;

		if ( widgit_portal_disable_gutenberg_for_post_type() ) {
			$disable = true;
		}

		if ( widgit_portal_disable_gutenberg_for_user_roles() ) {
			$disable = true;
		}

		if ( $disable ) {
			$gutenberg    = function_exists( 'gutenberg_register_scripts_and_styles' );
			$block_editor = has_action( 'enqueue_block_assets' );

			if ( $gutenberg || true === $block_editor ) {
				add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

				if ( $gutenberg ) {
					add_filter( 'gutenberg_can_edit_post_type', '__return_false', 100 );

					remove_action( 'admin_menu', 'gutenberg_menu' );
					remove_action( 'admin_init', 'gutenberg_redirect_demo' );

					remove_action( 'wp_enqueue_scripts', 'gutenberg_register_scripts_and_styles' );
					remove_action( 'admin_enqueue_scripts', 'gutenberg_register_scripts_and_styles' );
					remove_action( 'admin_notices', 'gutenberg_wordpress_version_notice' );
					remove_action( 'rest_api_init', 'gutenberg_register_rest_widget_updater_routes' );
					remove_action( 'admin_print_styles', 'gutenberg_block_editor_admin_print_styles' );
					remove_action( 'admin_print_scripts', 'gutenberg_block_editor_admin_print_scripts' );
					remove_action( 'admin_print_footer_scripts', 'gutenberg_block_editor_admin_print_footer_scripts' );
					remove_action( 'admin_footer', 'gutenberg_block_editor_admin_footer' );
					remove_action( 'admin_enqueue_scripts', 'gutenberg_widgets_init' );
					remove_action( 'admin_notices', 'gutenberg_build_files_notice' );

					remove_filter( 'load_script_translation_file', 'gutenberg_override_translation_file' );
					remove_filter( 'block_editor_settings', 'gutenberg_extend_block_editor_styles' );
					remove_filter( 'default_content', 'gutenberg_default_demo_content' );
					remove_filter( 'default_title', 'gutenberg_default_demo_title' );
					remove_filter( 'block_editor_settings', 'gutenberg_legacy_widget_settings' );
					remove_filter( 'rest_request_after_callbacks', 'gutenberg_filter_oembed_result' );
				}
			}
		}
	}
}
add_action( 'admin_init', 'widgit_portal_disable_gutenberg_init', 9 );


/**
 * Helper to determine whether or not to load the module
 *
 * @since       1.0.0
 * @return      bool $return Whether or not to load the module.
 */
function widgit_portal_disable_gutenberg_load() {
	$return = true;
	$compat = array(
		'classic-editor/classic-editor.php',
		'disable-gutenberg/disable-gutenberg.php',
	);

	foreach ( $compat as $plugin ) {
		if ( is_plugin_active( $plugin ) ) {
			$return = false;
		}
	}

	return apply_filters( 'widgit_portal_disable_gutenberg_load', $return );
}


/**
 * Get valid post types
 *
 * @since       1.0.0
 * @return      array $post_types An array of valid post types.
 */
function widgit_portal_disable_gutenberg_get_post_types() {
	$all_post_types     = get_post_types( array(), 'objects' );
	$post_types         = array();
	$invalid_post_types = apply_filters(
		'widgit_portal_disable_gutenberg_invalid_post_types',
		array(
			'attachment',
			'avia_framework_post',
			'custom_css',
			'customize_changeset',
			'frm_display',
			'frm_form_actions',
			'frm_styles',
			'nav_menu_item',
			'oembed_cache',
			'revision',
			'user_request',
			'wp_block',
			'wf_collection_policy',
			'wf_gf_entry',
			'wf_gf_form',
			'wf_plugin',
			'wf_user',
		)
	);

	foreach ( $all_post_types as $post_type ) {
		if ( ! in_array( $post_type->name, $invalid_post_types ) ) {
			$post_types[ $post_type->name ] = $post_type->label;
		}
	}

	asort( $post_types );

	return apply_filters( 'widgit_portal_disable_gutenberg_post_types', $post_types );
}


/**
 * Get roles
 *
 * @since       1.0.0
 * @return      array $roles An array of roles.
 */
function widgit_portal_disable_gutenberg_get_roles() {
	global $wp_roles;

	$roles = array();

	foreach ( $wp_roles->roles as $role => $role_meta ) {
		$roles[ $role ] = $role_meta['name'];
	}

	return $roles;
}


/**
 * Disable by post type
 *
 * @since       1.0.0
 * @return      bool $return Whether or not to load gutenberg.
 */
function widgit_portal_disable_gutenberg_for_post_type() {
	global $pagenow, $typenow;

	$post_type = null;
	$disabled  = widgit_portal()->settings->get_option( 'disable_gutenberg_for_post_types', array() );
	$return    = false;

	if ( 0 < $disabled ) {
		if ( 'edit.php' === $pagenow ) {
			$type_now  = empty( $typenow ) ? 'post' : $typenow;
			$post_type = $type_now;
		} elseif ( 'post.php' === $pagenow ) {
			$get       = wp_unslash( $_GET );
			$post_id   = isset( $get['post'] ) ? $get['post'] : null;
			$post_type = $post_id ? get_post_type( $post_id ) : null;
		} elseif ( 'post-new.php' === $pagenow ) {
			$type_now  = empty( $typenow ) ? 'post' : $typenow;
			$get       = wp_unslash( $_GET );
			$post_type = isset( $get['post_type'] ) ? $get['post_type'] : null;
			$post_type = empty( $post_type ) ? $type_now : $post_type;
		}

		if ( $post_type ) {
			if ( array_key_exists( $post_type, $disabled ) && 'checked' === $disabled[ $post_type ] ) {
				$return = true;
			}
		}
	}

	return apply_filters( 'widgit_portal_disable_gutenberg_for_post_type', $return, $post_type );
}


/**
 * Disable by user role
 *
 * @since       1.0.0
 * @return      bool $return Whether or not to load gutenberg.
 */
function widgit_portal_disable_gutenberg_for_user_roles() {
	$user     = wp_get_current_user();
	$roles    = (array) $user->roles;
	$disabled = widgit_portal()->settings->get_option( 'disable_gutenberg_for_user_roles', array() );
	$return   = false;

	foreach ( $roles as $role ) {
		if ( array_key_exists( $role, $disabled ) && 'checked' === $disabled[ $role ] ) {
			$return = true;
		}
	}

	return apply_filters( 'widgit_portal_disable_gutenberg_for_user_roles', $return, $roles );
}
