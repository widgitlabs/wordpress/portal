# Contribute To Portal

Community made patches, bug reports and contributions are always welcome!

When contributing please ensure you follow the guidelines below so that we can
keep on top of things.

## Getting Started

-   Submit a ticket for your issue, assuming one does not already exist.
    -   Raise it on our [Issue Tracker](https://gitlab.com/widgitlabs/wordpress/widgit-portal/issues)
    -   Clearly describe the issue including steps to reproduce the bug.
    -   Make sure you fill in the earliest version that you know has the issue as
        well as the version of WordPress you're using.

## Making Changes

-   Fork the repository on GitLab
-   Make the changes to your forked repository
    -   Ensure you stick to the [WordPress Coding Standards](https://codex.wordpress.org/WordPress_Coding_Standards)
-   When committing, reference your issue (if present) and include a note about
    the fix
-   If possible, and applicable, please also add/update unit tests for your changes
-   Push the changes to your fork and submit a merge request to the 'master' branch
    of the Simple Settings repository

## Code Documentation

-   We ensure that every function is documented well and follows the standards
    set by phpDoc
-   An example function can be found [here](https://gitlab.com/snippets/1981467)
-   Please make sure that every function is documented so that when we update our
    API Documentation things don't go awry!

At this point you're waiting on us to merge your request. We'll review all
merge requests, and make suggestions and changes if necessary.

### Additional Resources

-   [General GitLab Documentation](https://docs.gitlab.com/ee/user/)
-   [GitLab Merge Request Documentation](https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests)
-   [PHPUnit Tests Guide](https://phpunit.de/manual/current/en/writing-tests-for-phpunit.html)
